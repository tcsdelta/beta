const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb');

//port
const port = 4000; 

//Init app
const app = express();
const url = 'mongodb://ninja:ninja@localhost:27017/shrek';
const ObjectId = require('mongodb').ObjectID; //will be used to fetch document based on ObjectID

//body-parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

//View setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); //using ejs as view engine

//connect to mongodb
MongoClient.connect(url, (err, database) => {
    console.log('MongoDB connected...');
    if (err) throw err;

    db = database.db('shrek');
    Todos = db.collection('todos');

    app.listen(port, () => {
        console.log('Server running at port '+port);
    });
});

//Creating route for Index/home page
app.get('/', (req, res, next) => {
    Todos.find({}).toArray((err, allDocs) => { //toArray function will return all documents present in todos collection
        if(err) {
            return console.log(err);
       }
        res.render('index.ejs', {
            todos: allDocs  //providing [,locals] parameter to be used in view
        });
    });
});

app.post('/todo/add', (req, res, next) => {
    const todo = {

        text: req.body.todoText,
        body: req.body.todoBody
    }
    Todos.insert(todo, (err, result) => {
        if(err) {
            return console.log(err);
        }
        console.log('ToDo Added!');
        res.redirect('/');
        console.log(result);
    });
});

app.delete('/todo/delete/:id', (req, res, next) => {
    var Id = req.params.id;
    var query = {_id: ObjectId(Id)};
    console.log(Todos.deleteOne(query));
    Todos.deleteOne(query, (err, response) => {
        if(err) {
            return console.log(err);
       }
       console.log('ToDo removed');
       res.sendStatus(200);
    });
});

app.get('/todo/edit/:id', (req, res, next) => {
    var Id = req.params.id;
    var query = {_id: ObjectId(Id)};
    console.log(query);
    Todos.find(query).next((err, todo) => {
        if(err) {
            return console.log(err);
       }
        res.render('edit.ejs', {
            todo: todo
        });
    });
});

app.post('/todo/edit/:id', (req, res, next) => {
    var Id = req.params.id;
    var query = {_id: ObjectId(Id)};
    const todo = {
        text: req.body.todoText,
        body: req.body.todoBody
    }
    //update document
    Todos.updateOne(query, {$set:todo}, (err, result) => {
        if(err) {
            return console.log(err);
        }
        console.log('ToDo Updated!');
        res.redirect('/');
    });
});
