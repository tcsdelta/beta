$(document).ready(() => {
    $('.delete-todo').click(function(event) {   
        const id = event.target.getAttribute('data-id');
        $.ajax({
            type: 'DELETE',
            url: '/todo/delete/'+id,
            success: (response) => {
                alert('Deleting ToDo');
                window.location.href='/';
            },
            error: (error) => {
                console.log(error);
            }
        });
    });
});