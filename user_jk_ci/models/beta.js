const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Team beta Schema & Model
const BetaSchema = new Schema({
	name: {
		type: String,
		required:[true,'Name of the Beta Team-Member is required']
	},
	role: {
		type: String
	},
	location: {
		type: String
	},
	weeksActive: {
		type: Number,
		default: 0
	},
	assignmentsCompleted: {
		type: Number,
		default: 0
	},
	newIdeasImplemented: {
		type: Number,
		default: 0
	}
});

// Beta is the model and beta is the collection - ??
const Beta = mongoose.model('beta', BetaSchema);

// export the model
module.exports = Beta; 





