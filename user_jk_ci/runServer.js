const express = require('express');
// const routes = require('./routes/api.js');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// 1) base program is -->  runServer.js
// 2) api routes are in --> ./routes folder
// 3) calling the routes is prefixed as --> jk/api..


// Set-up exress app --> We now acquired a set of http functions 
const app = express();

//connect to mongoDb
//mongoose.connect('mongodb://localhost/shrek');
mongoose.connect('mongodb://ninja:ninja@localhost:27017/shrek',{useNewUrlParser: true});
//mongoose.connect('mongodb://ninja:ninja@localhost:9002/shrek');
//mongoose.connect('mongodb://ninja:ninja@localhost:9002/shrek',{useNewUrlParser: true});
//mongoose.connect('mongodb://ninja:ninja@3.16.136.6/shrek');
//mongoose.connect('mongodb://ninja:ninja@3.16.136.60:4000/shrek',{useNewUrlParser: true});
//mongoose.connect('mongodb://ninja:ninja@3.16.136.60:27017/shrek',{useNewUrlParser: true});
//mongoose.connect('mongodb:http://3.16.136.6/shrek');

//mongoose.connect('mongodb://ninja:ninja@3.16.136.6:4000/shrek',{useNewUrlParser: true});
//mongoose.connect('mongodb://ninja:ninja@3.16.136.60:4000/shrek').then(() => {
/*
mongoose.connect('mongodb://ninja:ninja@3.16.136.60:27017/shrek').then(() => {
	console.log("Connected to Database from AWS");
	}).catch((err) => {
	console.log("Not Connected to Database ERROR! ", err);
});
*/
mongoose.Promise = global.Promise; 

app.use(bodyParser.json());

// app.use('/jk/api', routes);
app.use('/jk/api', require('./routes/api.js'));


// Starting listening --> Not sure of the 4000 Port -- to explore on this
app.listen(process.env.port || 4000, function() {
	console.log('now started listening');
});






