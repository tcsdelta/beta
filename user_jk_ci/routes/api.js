const express = require('express');
const router = express.Router();
const Beta = require('../models/beta');

// start the routers handling part

router.get('/beta', function(req, res){
	res.send({type:'GET'});
});

router.post('/beta', function(req,res){
//	res.send({type:'POST'}); 
//      Not working as headers can't be set after request is sent --??	
//	console.log(req.body);

/*
	var beta = new Beta(req.body);
	beta.save(); //save it to DB
*/

   	// Beta.create(req.body); //using Mongoose method
	Beta.create(req.body).then(function(beta){
		res.send(beta);
	}); 
	
	/*
	res.send({
		type: 'POST',
		name: req.body.name,
		role: req.body.role,
		location: req.body.location,
		weeksActive: req.body.weeksActive,
		program: req.body.program
	});
	*/
});

router.put('/beta/:id', function(req,res){
	Beta.findByIdAndUpdate({_id: req.params.id},req.body).then(function(beta){
		Beta.findOne({_id: req.params.id}).then(function(beta){
			res.send(beta);
		});
                // res.send(beta); // Will not show latest one
        });
	// res.send({type:'PUT'});
});

router.delete('/beta/:id', function(req,res){
	// req.params.id
	Beta.findByIdAndRemove({_id: req.params.id}).then(function(beta){
		res.send(beta);
	});
	// res.send({type:'DELETE'});
});

module.exports = router;
